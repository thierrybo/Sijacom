unit uProcess;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, uTranslationStrings, jsonparser, JSONPropStorage, fpjson,
  StrUtils, LCLType, ComboEx, FileCtrl, CheckLst, uLog, regexpr;

procedure buildPage(fileName:string);

var
  chosenrootfile,chosenoutputdir,rootfileOnestring,mainPath,PrefAppLang,currentAppLang: string;
  rootfileJSON: TJSONData;
  buildFileArray: array of string;
  buildFileStringList,cmsKeyWordsArray,rootfile,appLangStringList: TStringList;

implementation

procedure buildPage(fileName:string);
var
  testString,cmsKeyWords,tempTag,tempGTag,tempCode,tempSCode,tagInsert,gTagInsert,tagJSONpath,gTagJSONpath,codeJSONpath,sCodeJSONpath,loadFilePath,inputFileOneString,insertFileOneString,outputString: string;
  i,j,tagFindCount,gTagFindCount,codeFindCount,sCodeFindCount,tempSCodeNr: integer;
  tagFindStop,gTagFindStop,codeFindStop,sCodeFindStop,linesEnd,testJSON: boolean;
  inputFile,insertFile,testFile,outputStringList,pageLinksStringList,sCodeJSONStringList: TStringList;
  tagPositions,gTagPositions,codePositions,sCodePositions: array of integer;
  tagArray,gTagArray,codeArray,sCodeArray: array of string;
begin
  log('Begin building file: ' + fileName,'START');;

  try
    cmsKeyWords := rootfileJSON.FindPath('general-info.tags').AsString;         // load a list of all tags used
  except
    On E:Exception do begin
      log('Error: Could not find JSON path: general-info.tags','file: ' + fileName);
    end;
  end;

  cmsKeyWordsArray := TStringList.Create;
  pageLinksStringList := TStringList.Create;
  outputString := '';

  cmsKeyWordsArray.Delimiter := ',';
  pageLinksStringList.Delimiter := ',';
  cmsKeyWordsArray.StrictDelimiter := True;
  pageLinksStringList.StrictDelimiter := True;
  cmsKeyWordsArray.DelimitedText := cmsKeyWords;

  inputFile := TStringList.Create;

  try
    inputFile.LoadFromFile(mainPath + 'input' + PathDelim + fileName); // load input file
  except
    On E:Exception do begin
      log('Error: Could not load file from ' + PathDelim + 'input' + PathDelim + ': ' + fileName,'ERROR');
    end;
  end;

  inputFileOneString := '';

  outputStringList := TStringList.Create;

  for i:=0 to inputFile.Count-1 do begin
    inputFileOneString := inputFileOneString + inputFile[i] + #10;              // The #10 symbol is used to mark line breaks.
  end;


  // SEARCH FOR %{CODES}%
  // We need to start with inserting the insert files' content because they may contain tags and gTags themselves

  tempCode := '';
  i:=0;
  linesEnd := false;

  while linesEnd = false do begin
    if (inputFileOneString[i] = '%') and (inputFileOneString[i+1] = '{') then begin // if there's the beginning of a code %{ somewhere
      codeFindStop := false;
      codeFindCount := i+1;                                                     // another code was found

      while codeFindStop = false do begin
        inc(codeFindCount);
        tempCode := tempCode + inputFileOneString[codeFindCount];

        if inputFileOneString[codeFindcount+1] = ' ' then begin                 // if a whitespace was found within the code...
          codeFindStop := true;                                                 // ...then the code is invalid and we'll look for the next code
          tempCode := '';
        end;

        if (inputFileOneString[codeFindcount+1] = '}') and (inputFileOneString[codeFindcount+2] = '%') then begin // if the code's end }% was found
          log('Found code: ' + tempCode,'in pos. ' + IntToStr(i));

          insertFile := TStringList.Create;
          insertFileOneString := '';
          codeJSONpath := '';

          Delete(inputFileOneString,i,length(tempCode)+4);
          insertFile.Clear;

          try
            codeJSONpath := 'replace.' + rootfileJSON.FindPath('pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.lang').AsString + '.' + tempCode;
            // replace.[lang].code
          except
            On E:Exception do begin
              log('Error: JSON path does not exist: ' + 'pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.lang','code: ' + tempCode);
            end;
          end;

          try
            loadFilePath := mainPath + PathDelim + 'insert' + PathDelim + rootfileJSON.FindPath(codeJSONpath).AsString; // get insert file's path
          except
            On E:Exception do begin
              log('Error: Could not load file from ' + PathDelim + 'insert' + PathDelim + ' ', 'JSON path: ' + codeJSONpath);
            end;
          end;

          try
            insertFile.LoadFromFile(loadFilePath);                              // load insert file
          except
            On E:Exception do begin
              log('Error: Could not load file: ' + loadFilePath,'code: ' + tempCode);
            end;
          end;

          for j:=0 to insertFile.Count-1 do begin
            insertFileOneString := insertFileOneString + insertFile[j] + #10;   // #10 is a line break delimiter
          end;

          Insert(insertFileOneString, inputFileOneString, i);                   // insert the insert file
          insertFileOneString := '';
          codeJSONpath := '';

          codeFindStop := true;
          tempCode := '';
          insertFile.Free;
        end;
      end;
    end;

    inc(i);
    if i > length(inputFileOneString) then linesEnd := true;                    // if the end of the input file is reached
  end;


  // SEARCH FOR %$SPECIALCODES$%

  tempSCode := '';
  i:=0;
  linesEnd := false;

  while linesEnd = false do begin
    if (inputFileOneString[i] = '%') and (inputFileOneString[i+1] = '$') then begin // if an sCode beginning %$ was found
      ScodeFindStop := false;
      ScodeFindCount := i+1;

      while sCodeFindStop = false do begin
        inc(sCodeFindCount);
        tempSCode := tempSCode + inputFileOneString[sCodeFindCount];

        if inputFileOneString[sCodeFindcount+1] = ' ' then begin                // if there's a whitespace within the sCode...
          sCodeFindStop := true;                                                // ...then the sCode is invalid
          tempSCode := '';
        end;

        if (inputFileOneString[sCodeFindcount+1] = '$') and (inputFileOneString[sCodeFindcount+2] = '%') then begin // if the sCode's end $% was found
          try
            tempSCodeNr := StrToInt(ReplaceRegExpr('.*(\d)$',tempSCode,'$1',true));
          except
            On E:Exception do begin
              log('Error: ' + ReplaceRegExpr('.*(\d)$',tempSCode,'$1',true) + ' is not an integer','sCode: ' + tempSCode);
            end;
          end;

          tempSCode := ReplaceRegExpr('(.*)\d$',tempSCode,'$1',true);           // get sCode number

          log('Found sCode: ' + tempSCode,'in pos. ' + IntToStr(i));

          insertFile := TStringList.Create;
          sCodeJSONStringList := TStringList.Create;
          sCodeJSONStringList.Delimiter := ',';
          sCodeJSONStringList.StrictDelimiter := true;

          try
            sCodeJSONStringList.DelimitedText := rootfileJSON.FindPath('pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.' + tempSCode).AsString;
                                                 // pages.[index].sCode
          except
            On E:Exception do begin
              log('Error: JSON path does not exist: ' + 'pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.' + tempSCode, 'sCode: ' + tempSCode);
            end;
          end;

          insertFileOneString := '';
          sCodeJSONpath := '';

          Delete(inputFileOneString,i,length(tempSCode)+5);                     // remove the sCode itself
          insertFile.Clear;

          if tempSCodeNr <= sCodeJSONStringList.Count-1 then begin              // if the sCode index is not out of bounds
            try
              sCodeJSONpath := 'replace.' + rootfileJSON.FindPath('pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.lang').AsString + '.' + sCodeJSONStringList[tempSCodeNr];
              // replace.[lang].sCode[nr]
              loadFilePath := mainPath + PathDelim + 'insert' + PathDelim + rootfileJSON.FindPath(sCodeJSONpath).AsString; // get insert file's path
            except
              On E:Exception do begin
                log('Error: JSON path does not exist: ' + sCodeJSONpath + '...', 'sCode: ' + tempSCode);
              end;
            end;

            try
              insertFile.LoadFromFile(loadFilePath);                            // load insert file
            except
              On E:Exception do begin
                log('Error: Could not load file: ' + loadFilePath,'sCode: ' + tempSCode);
              end;
            end;

            sCodeJSONStringList.Free;

            for j:=0 to insertFile.Count-1 do begin
              insertFileOneString := insertFileOneString + insertFile[j] + #10; // merge the insert file's lines into a single line
            end;

            Insert(insertFileOneString, inputFileOneString, i);                 // insert the insert file
          end else begin
            log('Error: sCode index out of bounds: ' + IntToStr(tempSCodeNr),tempSCode);
          end;

          insertFileOneString := '';
          sCodeJSONpath := '';

          sCodeFindStop := true;
          tempSCode := '';
          insertFile.Free;
        end;
      end;
    end;

    inc(i);
    if i > length(inputFileOneString) then linesEnd := true;                    // if the end of the input file is reached
  end;


  // SEARCH FOR %#TAGS#%

  tempTag := '';
  i:=0;
  linesEnd := false;

  while linesEnd = false do begin
    if (inputFileOneString[i] = '%') and (inputFileOneString[i+1] = '#') then begin // if tag beginning %# was found
      tagFindStop := false;
      tagFindCount := i+1;                                                      // another tag was found

      while tagFindStop = false do begin
        inc(tagFindCount);
        tempTag := tempTag + inputFileOneString[tagFindCount];

        if inputFileOneString[tagFindcount+1] = ' ' then begin                  // if there's a whitespace within the tag...
          tagFindStop := true;                                                  // ...then the tag is invalid
          tempTag := '';
        end;

        if (inputFileOneString[tagFindcount+1] = '#') and (inputFileOneString[tagFindcount+2] = '%') then begin // if the tag's end #% was found
          tagFindStop := true;
          log('Found tag: ' + tempTag,'in pos. ' + IntToStr(i));

          tagInsert := '';
          tagJSONpath := '';

          Delete(inputFileOneString,i,length(tempTag)+4);                       // remove the %#tag#% itself
          tagJSONpath := 'pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.' + tempTag;
                         // pages.[index].tag

          try
            tagInsert := rootfileJSON.FindPath(tagJSONpath).AsString;
            Insert(tagInsert, inputFileOneString, i);                           // insert tag into file
          except
            On E:Exception do begin
              log('JSON path does not exist: ' + tagJSONpath,'tag: ' + tempTag);
            end;
          end;

          tempTag := '';
        end;
      end;
    end;

    inc(i);
    if i > length(inputFileOneString) then linesEnd := true;                    // if the end of the input file is reached
  end;

  // SEARCH FOR %?gTags?%

  tempGTag := '';
  i:=0;
  linesEnd := false;

  while linesEnd = false do begin
    if (inputFileOneString[i] = '%') and (inputFileOneString[i+1] = '?') then begin // if gTag beginning %? was found
      gTagFindStop := false;
      gTagFindCount := i+1;                                                     // another gTag was found

      while gTagFindStop = false do begin
        inc(gTagFindCount);
        tempGTag := tempGTag + inputFileOneString[gTagFindCount];

        if inputFileOneString[gTagFindcount+1] = ' ' then begin                 // if there's a whitespace within the gTag...
          gTagFindStop := true;                                                 // ...then the gTag is invalid
          tempGTag := '';
        end;

        if (inputFileOneString[gTagFindcount+1] = '?') and (inputFileOneString[gTagFindcount+2] = '%') then begin // if the gTag end ?% was found
          gTagFindStop := true;
          log('Found gTag: ' + tempGTag,'in pos. ' + IntToStr(i));

          gTagInsert := '';
          gTagJSONpath := '';

          Delete(inputFileOneString,i,length(tempGTag)+4);                      // remove the %?gTag?% itself
          gTagJSONpath := 'general-tags.' + rootfileJSON.FindPath('pages.' + IntToStr(buildFileStringList.IndexOf(fileName)) + '.lang').AsString + '.' + tempGTag;
                          // general-tags.[lang].gTag
          try
            gTagInsert := rootfileJSON.FindPath(gTagJSONpath).AsString;
            Insert(gTagInsert, inputFileOneString, i);                          // insert gTag
          except
            On E:Exception do begin
              log('JSON path does not exist: ' + gTagJSONpath,'gTag: ' + tempGTag);
            end;
          end;

          tempGTag := '';
        end;
      end;
    end;

    inc(i);
    if i > length(inputFileOneString) then linesEnd := true;                    // if the end of the input file is reached
  end;

  outputString := inputFileOneString;                                           // load the adjusted input file string into the output file string

  outputStringList.Delimiter := #10;                                            // #10 are treated as line breaks
  outputStringList.StrictDelimiter := True;
  outputStringList.DelimitedText := outputString;
  outputStringList.SaveToFile(chosenoutputdir + PathDelim + fileName);                // put the output file into the output directory

  log('Built ' + fileName,'END');

  inputFile.Free;
  cmsKeyWordsArray.Free;
  pageLinksStringList.Free;
  outputStringList.Free;
end;

end.


unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, uTranslationStrings, jsonparser, JSONPropStorage, fpjson,
  StrUtils, LCLType, ComboEx, FileCtrl, CheckLst, Grids, Menus, uProcess, uLog,
  LCLTranslator, DefaultTranslator, uInfo;

type

  { TMainForm }

  TMainForm = class(TForm)
    BitBtnSelectAll: TBitBtn;
    BitBtnBuildAll: TBitBtn;
    BitBtnCancelBuild: TBitBtn;
    BitBtnChooseRoot: TBitBtn;
    BitBtnChooseOutputFolder: TBitBtn;
    BitBtnUnselectAll: TBitBtn;
    BitBtnSelectOutputFolder: TBitBtn;
    BitBtnSelectRoot: TBitBtn;
    CheckListBoxMain: TCheckListBox;
    Label1: TLabel;
    LblEdtRoot: TLabeledEdit;
    LblEdtOutputFolder: TLabeledEdit;
    MainMenu: TMainMenu;
    MenuInfo: TMenuItem;
    MenuDutch: TMenuItem;
    MenuIcelandic: TMenuItem;
    MenuBulgarian: TMenuItem;
    MenuDanish: TMenuItem;
    MenuFinnish: TMenuItem;
    MenuHungarian: TMenuItem;
    MenuItalian: TMenuItem;
    MenuFrench: TMenuItem;
    MenuLithuanian: TMenuItem;
    MenuLatvian: TMenuItem;
    MenuRomanian: TMenuItem;
    MenuSlovenian: TMenuItem;
    MenuSwedish: TMenuItem;
    MenuPolish: TMenuItem;
    MenuEstonian: TMenuItem;
    MenuSlovak: TMenuItem;
    MenuLang: TMenuItem;
    MenuEnglish: TMenuItem;
    MenuGerman: TMenuItem;
    OpenDlgMain: TOpenDialog;
    ProgressBarMain: TProgressBar;
    SelDirDlgMain: TSelectDirectoryDialog;
    TaskDialog1: TTaskDialog;
    procedure BitBtnBuildAllClick(Sender: TObject);
    procedure BitBtnCancelBuildClick(Sender: TObject);
    procedure BitBtnChooseOutputFolderClick(Sender: TObject);
    procedure BitBtnChooseRootClick(Sender: TObject);
    procedure BitBtnSelectAllClick(Sender: TObject);
    procedure BitBtnSelectOutputFolderClick(Sender: TObject);
    procedure BitBtnSelectRootClick(Sender: TObject);
    procedure BitBtnUnselectAllClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuBulgarianClick(Sender: TObject);
    procedure MenuDanishClick(Sender: TObject);
    procedure MenuDutchClick(Sender: TObject);
    procedure MenuEnglishClick(Sender: TObject);
    procedure MenuEstonianClick(Sender: TObject);
    procedure MenuFinnishClick(Sender: TObject);
    procedure MenuFrenchClick(Sender: TObject);
    procedure MenuGermanClick(Sender: TObject);
    procedure MenuHungarianClick(Sender: TObject);
    procedure MenuIcelandicClick(Sender: TObject);
    procedure MenuInfoClick(Sender: TObject);
    procedure MenuItalianClick(Sender: TObject);
    procedure MenuLatvianClick(Sender: TObject);
    procedure MenuLithuanianClick(Sender: TObject);
    procedure MenuPolishClick(Sender: TObject);
    procedure MenuRomanianClick(Sender: TObject);
    procedure MenuSlovakClick(Sender: TObject);
    procedure MenuSlovenianClick(Sender: TObject);
    procedure MenuSwedishClick(Sender: TObject);
  private

  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }


// DISPLAY FILES TO BE BUILT
procedure displayAvailablePages();
var i,pagecount:integer; jsonpath, buildfilename: string;
begin
  buildFileStringList.Clear;                                                    // when loading another root file then all in-app data
  MainForm.CheckListBoxMain.Items.Clear;                                        // about the old root file needs to be cleared
  setLength(buildfilearray, 0);
  MainForm.ProgressBarMain.Position := 0;

  pagecount := StrToInt(rootfileJSON.FindPath('general-info.pagecount').AsJSON);// total number of the pages

  for i:=0 to pagecount-1 do begin
    jsonpath := 'pages.' + IntToStr(i) + '.file';                               // get file names of all pages
    buildfilename := rootfileJSON.FindPath(jsonpath).AsString;
    MainForm.CheckListBoxMain.Items.Add(buildfilename);                         // add each of those file names to the CheckListBox...

    setLength(buildfilearray, length(buildfilearray)+1);                        // ...and to the buildfile array
    buildfilearray[i] := buildfilename;
    buildFileStringList.Add(buildFileName);                                     // ...as well as to the buildfile StringList
  end;
end;



// SELECT ROOT JSON FILE
procedure TMainForm.BitBtnChooseRootClick(Sender: TObject);
begin
  OpenDlgMain.DefaultExt := '.sijacom';                                         // only display .sijacom files to choose from
  OpenDlgMain.Filter := 'Sijacom file|*.sijacom';
  OpenDlgMain.Title := i18nLoadFile;

  if OpenDlgMain.Execute then begin
    if fileExists(OpenDlgMain.Filename) then begin
       LblEdtRoot.Text := OpenDlgMain.Filename;                                 // put the root file's name into the LabelEdit
       mainPath := ExtractFilePath(OpenDlgMain.Filename);                       // main path is needed to find the /input/ and /insert/ directory later
    end else begin
      Application.MessageBox(PChar(i18nFileDoesntExist),PChar(i18nError), MB_ICONERROR);
    end;
  end else begin
    Application.MessageBox(PChar(i18nNoFileSelected),PChar(i18nError), MB_ICONERROR);
  end;
end;


// SELECT ALL FILES
procedure TMainForm.BitBtnSelectAllClick(Sender: TObject);
var i: integer;
begin
  for i:=0 to CheckListBoxMain.Items.Count-1 do begin
    CheckListBoxMain.Checked[i] := true;                                        // set all build files' checkboxes to checked
  end;
end;


// UNSELECT ALL FILES
procedure TMainForm.BitBtnUnselectAllClick(Sender: TObject);
var i:integer;
begin
  for i:=0 to CheckListBoxMain.Items.Count-1 do begin
    CheckListBoxMain.Checked[i] := false;                                       // set all build files' checkboxes to unchecked
  end;
end;


// CONFIRM ROOT FILE
procedure TMainForm.BitBtnSelectRootClick(Sender: TObject);
var i: integer;
begin
  rootfile := TStringList.Create;

  try
    if fileExists(LblEdtRoot.Text) then begin                                   // if the file in the LabelEdit exists...
      chosenrootfile := LblEdtRoot.Text;                                        // ...load its path into the chosenrootfile variable
    end else begin
      Application.MessageBox(PChar(i18nFileDoesntExist),PChar(i18nError),MB_ICONERROR);
      exit;
    end;

    try
      rootfile.LoadFromFile(chosenrootfile);                                    // load chosen root file into "rootfile" variable
    except
      On E:Exception do begin
        Application.MessageBox(PChar(E.Message),PChar(i18nError),MB_ICONERROR);
      end;
    end;

    rootfileOnestring := '';

    for i:=0 to rootfile.Count - 1 do begin
      rootfileOnestring := rootfileOnestring + rootfile[i];                     // merge all lines of the root file into one string
    end;

    try
      rootfileJSON := GetJSON(rootfileOnestring);                               // get JSON from OneString file variable
      displayAvailablePages();                                                  // show all available page files in the CheckListBox

      Application.MessageBox(PChar(i18nRootFileLoaded),PChar(i18nSuccess),MB_ICONINFORMATION);
    except
      On E:Exception do begin
        Application.MessageBox(PChar(E.Message),PChar(i18nError),MB_ICONERROR);
      end;
    end;

  finally
    rootfile.Free;
  end;
end;


// WHEN FORM IS CREATED
procedure TMainForm.FormCreate(Sender: TObject);
var i:integer;
begin
  buildFileStringList := TStringList.Create;                                    // we need this StringList every time a root file is loaded but we can't create the StringList at OnClick because we don't know whether it already exists
  appLangStringList := TStringList.Create;

  try
    appLangStringList.LoadFromFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\config\lang.txt');
  except
    On E:Exception do begin
      appLangStringList.Clear;
      appLangStringList.Add('en');
    end;
  end;

  try
    SetDefaultLang(appLangStringList[0]);
  except
    On E:Exception do begin
      // currently nothing
    end;
  end;

  appLangStringList.Free;

  if ParamCount > 0 then begin
    if ExtractFileExt(ParamStr(1)) = '.sijacom' then begin
      rootfile := TStringList.Create;

      try
        if fileExists(ParamStr(1)) then begin                                   // if the file in the LabelEdit exists...
          chosenrootfile := ParamStr(1);                                        // ...load its path into the chosenrootfile variable
          LblEdtRoot.Text := ParamStr(1);
          mainPath := ExtractFilePath(ParamStr(1));
        end else begin
          Application.MessageBox(PChar(i18nFileDoesntExist),PChar(i18nError),MB_ICONERROR);
          exit;
        end;

        try
          rootfile.LoadFromFile(ParamStr(1));                                   // load chosen root file into "rootfile" variable
        except
          On E:Exception do begin
            Application.MessageBox(PChar(E.Message),PChar(i18nError),MB_ICONERROR);
          end;
        end;

        rootfileOnestring := '';

        for i:=0 to rootfile.Count - 1 do begin
          rootfileOnestring := rootfileOnestring + rootfile[i];                 // merge all lines of the root file into one string
        end;

        try
          rootfileJSON := GetJSON(rootfileOnestring);                           // get JSON from OneString file variable
          displayAvailablePages();                                              // show all available page files in the CheckListBox

          Application.MessageBox(PChar(i18nRootFileLoaded),PChar(i18nSuccess),MB_ICONINFORMATION);
        except
          On E:Exception do begin
            Application.MessageBox(PChar(E.Message),PChar(i18nError),MB_ICONERROR);
          end;
        end;

      finally
        rootfile.Free;
      end;
    end else begin
      Application.MessageBox(PChar(i18nUnsupportedFileType),PChar(i18nError),MB_ICONERROR);
    end;
  end;
end;


// WHEN FORM IS DESTROYED
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  buildFileStringList.Free;                                                     // we need this StringList during the whole runtime so it's only freed when the program is stopped

  appLangStringList := TStringList.Create;
  appLangStringList.Add(currentAppLang);

  try
    appLangStringList.SaveToFile(ExpandFileName(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName))) + '\config\lang.txt');
  except
    On E:Exception do begin
      // currently nothing
    end;
  end;

  appLangStringList.Free;
end;


// SET ENGLISH
procedure TMainForm.MenuEnglishClick(Sender: TObject);
begin
  SetDefaultLang('en');                                                         // change language to English
  currentAppLang := 'en';
end;

// SET DANISH
procedure TMainForm.MenuDanishClick(Sender: TObject);
begin
  SetDefaultLang('da');
  currentAppLang := 'da';
end;

// SET GERMAN
procedure TMainForm.MenuGermanClick(Sender: TObject);
begin
  SetDefaultLang('de');
  currentAppLang := 'de';
end;

// SET HUNGARIAN
procedure TMainForm.MenuHungarianClick(Sender: TObject);
begin
  SetDefaultLang('hu');
  currentAppLang := 'hu';
end;

// SET ICELANDIC
procedure TMainForm.MenuIcelandicClick(Sender: TObject);
begin
  SetDefaultLang('is');
  currentAppLang := 'is';
end;

// SET BULGARIAN
procedure TMainForm.MenuBulgarianClick(Sender: TObject);
begin
  SetDefaultLang('bg');
  currentAppLang := 'bg';
end;

// SET SLOVAK
procedure TMainForm.MenuSlovakClick(Sender: TObject);
begin
  SetDefaultLang('sk');
  currentAppLang := 'sk';
end;

// SET SLOVENIAN
procedure TMainForm.MenuSlovenianClick(Sender: TObject);
begin
  SetDefaultLang('sl');
  currentAppLang := 'sl';
end;

// SET SWEDISH
procedure TMainForm.MenuSwedishClick(Sender: TObject);
begin
  SetDefaultLang('sv');
  currentAppLang := 'sv';
end;

// SET DUTCH
procedure TMainForm.MenuDutchClick(Sender: TObject);
begin
  SetDefaultLang('nl');
  currentAppLang := 'nl';
end;

// SET POLISH
procedure TMainForm.MenuPolishClick(Sender: TObject);
begin
  SetDefaultLang('pl');
  currentAppLang := 'pl';
end;

// SET ROMANIAN
procedure TMainForm.MenuRomanianClick(Sender: TObject);
begin
  SetDefaultLang('ro');
  currentAppLang := 'ro';
end;

// SET ESTONIAN
procedure TMainForm.MenuEstonianClick(Sender: TObject);
begin
  SetDefaultLang('et');
  currentAppLang := 'et';
end;

// SET FINNISH
procedure TMainForm.MenuFinnishClick(Sender: TObject);
begin
  SetDefaultLang('fi');
  currentAppLang := 'fi';
end;

// SET FRENCH
procedure TMainForm.MenuFrenchClick(Sender: TObject);
begin
  SetDefaultLang('fr');
  currentAppLang := 'fr';
end;

// SET ITALIAN
procedure TMainForm.MenuItalianClick(Sender: TObject);
begin
  SetDefaultLang('it');
  currentAppLang := 'it';
end;

// SET LATVIAN
procedure TMainForm.MenuLatvianClick(Sender: TObject);
begin
  SetDefaultLang('lv');
  currentAppLang := 'lv';
end;

// SET LITHUANIAN
procedure TMainForm.MenuLithuanianClick(Sender: TObject);
begin
  SetDefaultLang('lt');
  currentAppLang := 'lt';
end;


// OPEN INFO
procedure TMainForm.MenuInfoClick(Sender: TObject);
begin
  InfoForm.Show;                                                                // display the Info window
end;


// CHOOSE OUTPUT FOLDER
procedure TMainForm.BitBtnChooseOutputFolderClick(Sender: TObject);
var chosenOutputDirectory: string;
begin
  SelectDirectory(i18nChooseFolder,'',chosenOutputDirectory);                   // display select directory dialog with chosenOutputDirectory as the result
  LblEdtOutputFolder.Text := chosenOutputDirectory;                             // put the result into the output folder LabelEdit
end;


// SELECT OUTPUT FOLDER
procedure TMainForm.BitBtnSelectOutputFolderClick(Sender: TObject);
begin
  if directoryExists(LblEdtOutputFolder.Text) then begin                        // if the directory in the output folder LabelEdit exists...
    chosenoutputdir := LblEdtOutputFolder.Text;                                 // ...load it into the variable chosenoutputdir
    Application.MessageBox(PChar(i18nDirSelected),PChar(i18nSuccess),MB_ICONINFORMATION);
  end else begin
    Application.MessageBox(PChar(i18nFolderDoesntExist),PChar(i18nError), MB_ICONERROR);
  end;
end;


// BUILD FILES
procedure TMainForm.BitBtnBuildAllClick(Sender: TObject);
var i,checkCount:integer;
begin
  if (chosenrootfile = '') or (chosenoutputdir = '') then begin                 // if the user didn't select a root file or an output folder
    Application.MessageBox(PChar(i18nNoRootOrFolder),PChar(i18nError), MB_ICONERROR);
    exit;
  end;

  with TTaskDialog.Create(self) do                                              // ask the user if he wants to build all files
      try
        Title := i18nBuildFiles;
        Caption := i18nConfirm;
        Text := i18nBuildFilesDesc;
        CommonButtons := [];
        with TTaskDialogButtonItem(Buttons.Add) do
        begin
          Caption := i18nConfirm;
          ModalResult := mrYes;
        end;
        with TTaskDialogButtonItem(Buttons.Add) do
        begin
          Caption := i18nCancel;
          ModalResult := mrNo;
        end;
        MainIcon := tdiWarning;
        if Execute then
          if ModalResult = mrNo then begin
            exit;
          end;
      finally
        Free;
      end;

  checkCount := 0;                                                              // total number of selected build files
  ProgressBarMain.Position := 0;                                                // set the progress bar to 0
  FormLog.Show;                                                                 // display the log window
  clearLog();                                                                   // clear the (last) log

  for i:=0 to CheckListBoxMain.Items.Count-1 do begin
     if CheckListBoxMain.Checked[i] then begin
       inc(checkCount);                                                         // count how many build files the user selected
     end;
  end;

  for i:=0 to CheckListBoxMain.Items.Count-1 do begin                           // for each build file
    if CheckListBoxMain.Checked[i] then begin                                   // if the build file is selected...
      buildPage(CheckListBoxMain.Items[i]);                                     // ...build the page
      if i<CheckListBoxMain.Items.Count-1 then log('','');                      // empty line between each page's log
    end;

    if checkCount <> 0 then ProgressBarMain.Position := Round(((i+1) / checkCount) * 100); // calculate percentage of built filesb but avoiding division by 0

    Application.ProcessMessages;                                                // This tells Windows that Sijacom is not frozen but actually calculates. Also this lets Sijacom handle button clicks during the building process.
  end;

  if checkCount = 0 then begin                                                  // if the user didn't select any files to build
    Application.MessageBox(PChar(i18nNoFileSelected),PChar(i18nError), MB_ICONERROR);
  end else begin
    Application.MessageBox(PChar(i18nFinished),PChar(i18nSuccess), MB_ICONINFORMATION);
  end;
end;


// EMERGENCY STOP
procedure TMainForm.BitBtnCancelBuildClick(Sender: TObject);
begin
  Close;                                                                        // In an Emergency just stop the program...
end;

end.


unit uLog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  Grids, ValEdit,LCLTranslator,uTranslationStrings;

type

  { TFormLog }

  TFormLog = class(TForm)
    BitBtnCloseLog: TBitBtn;
    StringGridLog: TStringGrid;
    procedure BitBtnCloseLogClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StringGridLogPrepareCanvas(sender: TObject; aCol, aRow: Integer;
      aState: TGridDrawState);
  private

  public

  end;

var
  FormLog: TFormLog;
  logCount: integer;

procedure log(logText,logRef:string);
procedure clearLog();

implementation

{$R *.lfm}

procedure log(logText,logRef:string);
begin
  if logCount <> 0 then FormLog.StringGridLog.RowCount := FormLog.StringGridLog.RowCount + 1; // add another line if there are already lines
  FormLog.StringGridLog.Cells[0,FormLog.StringGridLog.RowCount-1] := IntToStr(FormLog.StringGridLog.RowCount-1); // put the log number into the first column
  FormLog.StringGridLog.Cells[1,FormLog.StringGridLog.RowCount-1] := logText;   // put the log text into the second column
  FormLog.StringGridLog.Cells[2,FormLog.StringGridLog.RowCount-1] := logRef;    // put the log reference into the third column
  inc(logCount);
end;

procedure clearLog();
begin
  FormLog.StringGridLog.RowCount := 2;                                          // remove all rows except the title row and the first row
  FormLog.StringGridLog.Cells[0,1] := '';                                       // empty the first row's columns
  FormLog.StringGridLog.Cells[1,1] := '';
  FormLog.StringGridLog.Cells[2,1] := '';
end;

{ TFormLog }

procedure TFormLog.BitBtnCloseLogClick(Sender: TObject);
begin
  FormLog.Hide;                                                                 // close the log window
end;

procedure TFormLog.FormCreate(Sender: TObject);
begin
  logCount := 0;                                                                // set the log count to 0 when the window is opened
end;

procedure TFormLog.StringGridLogPrepareCanvas(sender: TObject; aCol,
  aRow: Integer; aState: TGridDrawState);
var ATextStyle: TTextStyle;
begin
  // This stuff makes that every row has 2 text lines (for longer log strings)
  // I don't really know how it works, I stole it from Stack Overflow :D
  ATextStyle := StringGridLog.Canvas.TextStyle;
  ATextStyle.SingleLine := false;
  ATextStyle.Wordbreak := true;
  StringGridLog.Canvas.TextStyle := ATextStyle;
end;

end.


��    ?        Y         p  �   q     q     s     �  	   �     �     �  	   �  #   �  %   �               6     G     ]  	   c     m  
   u  0   �  (   �  +   �                         #     0     5     I     _     t     {     �     �     �     �     �  	   �     �  	   �     �     �  	   �     	     	     	     	  
   &	     1	  
   :	     E	     M	     T	     \	  4   i	     �	  4   �	  ,   �	  1   
  )   I
     s
     �
  ~  �
  �       �     �  C   �     �  '     )   F     p  6   �  7   �     �  *   �     *  +   J     v     �     �     �  \   �  L   #  Q   p     �     �     �     �            "   !  $   D     i     ~     �     �     �     �  0   �     �  	   �     �       (   4     ]  	   x     �     �     �  -   �     �     �                &  
   7  !   B  X   d     �     �     �     �     �               /   "      +              3   4   ?       -      #   7         ,   .      0           '   !         1      *      9                  	   )   $   <       ;                                %       :         
       6                                       >             =   8                             2       &   (       5    "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-27 16:07+0100
Last-Translator: pixelcode <pixelcode@dismail.de>
Language-Team: Bulgarian <https://weblate.bubu1.eu/projects/sijacom/sijacom/bg/>
Language: bg
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" е проста 
генератор на статични уебсайтове, чиято цел е да 
опрости процеса на приспособяване на статичните 
елементи на уебсайта в множество подстраници. 
Моля, направете справка с ръководството за
 , За Sijacom Всички файлове са изградени успешно! Изградете всички Файлове за изработка: Изграждане на файлове? български C:\documents\МоятСмешенПроект\\output\ C:\documents\МоятСмешенПроект\root.json Отмени Изберете изходна папка Изберете каталог Изберете изходна папка? Затвори Съставител: Потвърдете Програмист: Наистина ли искате да изберете тази изходна папка? Искате ли да изградите избраните файлове? Искате ли да заредите избрания основен файл? Холандски Английски Грешка Естонски Fatcow Икони Файл Не съществува файл Папка не съществува Free Pascal Compiler Немски Икони: Запазете Език LiJu09 Зареждане на главния файл? Вход Mondstern Няма избран файл! Отворен файл Избрана изходна папка Изходна папка: Pixelcode Полски Реф. Заменете Коренният файл е зареден Коренен файл: SOS спирка Изберете всички Sijacom Словакия Успех Отбележете всички Трябва да изберете основен файл и изходна папка! Инфо Изберете... Изберете... Изберете Изберете Инфо v1.1 
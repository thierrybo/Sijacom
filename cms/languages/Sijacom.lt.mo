��    G      T  a   �        �                  !  	   ?     I     V  	   c  #   m  %   �     �     �     �     �     �  	               
     0   '  (   X  +   �     �     �     �     �     �     �     �     �     		     	     '	     <	  	   C	     M	     T	     Y	     b	     i	     y	  	   }	     �	  	   �	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     
  
   
     
     
  	   &
     0
     8
     @
     ]
     j
  4   w
     �
  4   �
  ,   �
  1   %  )   W     �     �  �  �    �     �     �     �     �     �     �       +   
  -   6  	   d     n     �     �  	   �     �     �     �  
   �  5   �  #   0  /   T     �     �     �     �     �     �     �     �     �     �     �  	             '     /     7     =     D     ^  	   j     t     �     �     �  	   �     �     �     �     �               '     9     A  	   J     T     \     d  
   �     �  <   �     �     �     �                    '        3                   @       9       2   F   G         6   /      ?   ,   A           
   D   '          8      >   	       +            :   1         %              "   &           <   )           C                         .       4         ;   #                  5   =   E   0      (                       B              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-30 16:03+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Lithuanian <https://weblate.bubu1.eu/projects/sijacom/sijacom/lt/>
Language: lt
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && (n % 100 < 11 || n % 100 > 19)) ? 0 : ((n % 10 >= 2 && n % 10 <= 9 && (n % 100 < 11 || n % 100 > 19)) ? 1 : 2);
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" yra 
paprastas statinių svetainių generatorius, kurio tikslas - 
 supaprastinti statinių svetainių elementų koregavimo 
procesą visoje keliuose tinklalapiuose. Žr. vadovą 
informacijos, kaip naudotis "Sijacom".
 , Apie "Sijacom Visi failai sėkmingai sukurti! Sukurti visus Sukurti failus: Sukurti failus? Bulgarų C:\documents\ManoJuokingasProjektas\output\ C:\documents\ManoJuokingasProjektas\root.json Atšaukti Pasirinkite išvesties aplanką Pasirinkite katalogą Pasirinkti išvesties aplanką? Uždaryti Sudarytojas: Patvirtinkite Danų Kūrėjas: Ar tikrai norite pasirinkti šį išvesties aplanką? Ar norite kurti pasirinktus failus? Ar norite įkelti pasirinktą šakninį failą? Olandų Anglų Klaida Estų Fatcow piktogramos Failas Failas neegzistuoja Failų tipas nepalaikomas! Suomių Aplanko nėra Free Pascal Compiler Vokiškas Islandų Ikonos: Laikyti Kalba LiJu09 Įkelti šakninį failą? Registruoti Mondstern Nėra pasirinkto failo! Atidaryti failą Parinktas išvesties aplankas Išvesties aplankas: Pixelcode Lenkų Nr. Pakeisti Įkeltas šakninis failas Šakninis failas: SOS stotelė Pasirinkite visus Sijacom Slovakų Slovėnų Sėkmė Švedų Išversti Sijacom į Weblate Vertėjai: Atšaukti visus Turite pasirinkti pagrindinį failą ir išvesties aplanką! Informacija Pasirinkite... Pasirinkite... Pasirinkite Pasirinkite Informacija v1.1 
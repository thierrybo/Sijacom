��    G      T  a   �        �                  !  	   ?     I     V  	   c  #   m  %   �     �     �     �     �     �  	               
     0   '  (   X  +   �     �     �     �     �     �     �     �     �     		     	     '	     <	  	   C	     M	     T	     Y	     b	     i	     y	  	   }	     �	  	   �	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     
  
   
     
     
  	   &
     0
     8
     @
     ]
     j
  4   w
     �
  4   �
  ,   �
  1   %  )   W     �     �  �  �  u  |     �     �  #         $     0     B     Q  -   Z  /   �     �     �     �     �               $     1     :  4   K  )   �  /   �     �     �     �     �     �       	             8  	   =     G     \     b     n     v     �     �     �     �  	   �     �     �     �       	             !     %     .     D     R     ^     m     u  	   �  	   �     �     �     �     �  0   �                    /     8     A     N        3                   @       9       2   F   G         6   /      ?   ,   A           
   D   '          8      >   	       +            :   1         %              "   &           <   )           C                         .       4         ;   #                  5   =   E   0      (                       B              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-30 16:04+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Latvian <https://weblate.bubu1.eu/projects/sijacom/sijacom/lv/>
Language: lv
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 0 || n % 100 >= 11 && n % 100 <= 19) ? 0 : ((n % 10 == 1 && n % 100 != 11) ? 1 : 2);
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" ir 
vienkārša savienošanas un satura pārvaldības 
programma. statisko tīmekļa vietņu ģenerators, kura 
mērķis ir vienkāršot tīmekļa vietņu pārvaldību. statisko 
tīmekļa vietņu elementu pielāgošanas procesu visā 
vairākās apakšlapās. Lūdzu, skatiet rokasgrāmatu 
informāciju par to, kā lietot Sijacom.
 , Par Sijacom Visi faili ir veiksmīgi izveidoti! Veidot visu Būvējami faili: Veidot failus? Bulgāru C:\documents\MansSmieklīgaisProjekts\output\ C:\documents\MansSmieklīgaisProjekts\root.json Atcelt Izvēlieties izejas mapi Izvēlieties direktoriju Izvēlēties izejas mapi? Aizvērt Sastādītājs: Apstipriniet Dānijas Izstrādātājs: Vai tiešām vēlaties izvēlēties šo izejas mapi? Vai vēlaties izveidot atlasītos failus? Vai vēlaties ielādēt atlasīto saknes failu? Holandiešu Angļu Kļūdas Igauņu Fatcow ikonas Faili Faila nav Faila tips nav atbalstīts! Somu Mapes nav Free Pascal Compiler Vācu Islandiešu Ikonas: Saglabājiet Valoda LiJu09 Ielādēt sakņu failu? Reģistrēt Mondstern Nav atlasīts neviens fails! Atvērt failu Izvēlēta izejas mape Izvades mape: Pixelcode Polijas Sk. Aizstāt Iekrauts saknes fails Sakņu fails: SOS pietura Atlasiet visus Sijacom Slovākijas Slovēņu Panākumi Zviedru Tulkot Sijacom par Weblate Tulkotāji: Atcelt visus Jums ir jāizvēlas saknes fails un izejas mape! Informācija Izvēlieties... Izvēlieties... Atlasiet Atlasiet Informācija v1.1 
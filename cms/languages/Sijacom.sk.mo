��    :      �  O   �      �  �   �     �       	   %     /     <  #   I  %   m     �     �     �     �     �  	   �     �  
   �  0   �  (   -  +   V     �     �     �     �     �     �     �     �     �     �     �     �                       	   2     <     S  	   b     l     s     x     �  
   �     �  
   �     �     �     �     �  4   �     		  4    	  ,   U	  1   �	  )   �	     �	  �  �	  /  �     �  *   �     �          (  )   ;  +   e     �     �     �     �  	   �     �  	   �  
   	  2     "   G  -   j  
   �     �     �     �     �     �     �     �     �            	   "     ,     2     P     X     x     �     �  	   �     �     �  	   �     �                     ,     4     @     H  =   [     �  
   �  
   �     �     �     �     $      3          *             -   :      7   6   /   %   '      #                                  4       !   "                 5      1      &                           (          +   ,          .   9   0                 )   
       	   2                                  8        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 About Sijacom All files built successfully! Build all Build files: Build files? C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language Load root file? Log No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 2021-08-26 15:04+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Slovak <https://weblate.bubu1.eu/projects/sijacom/sijacom/sk/>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.5.7
 "Simple Joining And COntent Management" je 
 jednoduchý generátor statických webových stránok, 
 ktorého účelom je zjednodušiť proces úpravy statických 
 prvkov webových stránok v rámci viacerých podstránok. 
 Pozrite si návod na obsluhu informácie o ovládaní 
 aplikácie Sijacom.
 O Sijacom-e Všetky súbory boli úspešne zostavené! Zostaviť všetky Súbory na zostavenie: Zostaviť súbory? C:\documents\MôjZábavnýProjekt\output\ C:\documents\MôjZábavnýProjekt\root.json Zrušiť Vyberte výstupný priečinok Vyberte adresár Vybrať výstupný priečinok? Zatvoriť Kompilátor: Potvrdiť Vývojár: Naozaj chcete vybrať tento výstupný priečinok? Chcete zostaviť vybrané súbory? Chcete načítať vybraný koreňový súbor? Holandský Angličtina Chyba Estónčina Ikony Fatcow Súbor Súbor neexistuje Priečinok neexistuje Free Pascal Compiler Nemčina Ikony: Udržujte Jazyk Načítať koreňový súbor? Záznam Nie je vybraný žiadny súbor! Otvoriť súbor Vybraný výstupný priečinok Výstupný priečinok: Pixelcode Poľský Odkaz. Nahradiť Koreňový súbor načítaný Koreňový súbor: SOS stop Vybrať všetky Sijacom Slovenčina Úspech Odznačiť všetko Musíte si vybrať koreňový súbor a výstupný priečinok! Info Vybrať... Vybrať... Zvoliť Zvoliť Info 
��    @        Y         �  �   �     �     �  	   �     �     �  #   �  %   �          "     :     K     a  	   g     q  
   y  0   �  (   �  +   �     
                    '     4     9     M     f     |     �     �     �     �     �     �     �  	   �     �  	   �     �     	  	   	     	     %	     *	     2	  
   C	     N	  
   W	     b	     j	     q	     y	     �	     �	  4   �	     �	  4   �	  ,   1
  1   ^
  )   �
     �
     �
  {  �
  �   [     Z  (   h     �     �     �  (   �  *   �  	        )     E     W  
   o     z     �     �  7   �  5   �  0        >     N     W     ^     g     t     z     �     �     �     �     �     �     �     �        	     	        '     @     N     i  	   y     �     �     �     �     �  	   �     �     �  
   �     �     �            >   .     m  
   r  
   }  	   �  	   �     �     �     .   !      *              2   3   6       ,      "   8         @   -      /   <       &       	      0      )      :                 5   (   #   =       7                               $       ;                                            +            ?             >   9                          
   1       %   '       4    "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 About Sijacom All files built successfully! Build all Build files: Build files? C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Folder does not exist Free Pascal Compiler German Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 2021-08-26 23:46+0100
Last-Translator: pixelcode <pixelcode@dismail.de>
Language-Team: German <https://weblate.bubu1.eu/projects/sijacom/sijacom/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
 "Simple Joining and COntent Management" ist ein 
einfacher Generator statischer Websites, der es 
erleichtern soll, gleichbleibende Website-Elemente 
über verschiedene Unterseiten hinweg abzuändern. 
Wie du Sijacom bedienst, erfährst du im Handbuch. 
 Über Sijacom Alle Dateien erfolgreich zusammengebaut! Alles bauen Dateien zusammenbauen: Dateien zusammenbauen? C:\documents\MeinLustigesProject\output\ C:\documents\MeinLustigesProject\root.json Abbrechen Wähle einen Ausgabe-Ordner Ordner auswählen Ausgabe-Ordner wählen? Schließen Kompilierer: Bestätigen Entwickler: Möchtest du wirklich diesen Ausgabe-Ordner auswählen? Möchtest du die ausgewählten Dateien zusammenbauen? Möchtest du die ausgewählte Wurzeldatei laden? Niederländisch Englisch Fehler Estnisch Fatcow Icons Datei Datei existiert nicht Dateityp nicht unterstützt! Ordner existiert nicht Free Pascal Compiler Deutsch Icons: Behalten Sprache LiJu09 Wurzeldatei laden? Protokoll Mondstern Keine Datei ausgewählt! Datei öffnen Ausgabe-Ordner ausgewählt Ausgabe-Ordner: Pixelcode Polnisch Referenz Ersetzen Wurzeldatei geladen Wurzeldatei: SOS-Stopp Alle auswählen Sijacom Slowakisch Erfolg Übersetze Sijacom auf Weblate Übersetzer: Alle abwählen Du musst eine Wurzeldatei und einen Ausgabe-Ordner auswählen! Info Wählen... Wählen... Festlegen Festlegen Info v1.1 
��    @        Y         �  �   �     �     �     �  	   �     �     �  	   �  #   �  %        '     .     F     W     m  	   s     }     �  
   �  0   �  (   �  +   �          #     +     1     :     G     L     `     v     �     �     �     �     �     �     �  	   �     �  	   �     �     �  	   	     	     	     $	     ,	  
   =	     H	  
   Q	     \	     d	     k	     s	  4   �	     �	  4   �	  ,   
  1   .
  )   `
     �
     �
  |  �
    ,     K     M  -   ]     �     �     �  	   �  )   �  +   �     $     -     D     V     n     t     }     �     �  0   �  $   �  '   �          "     +     1     7  
   E     P     a     q     �     �     �     �     �     �     �  	   �     �     �     �       	        !     (     .     5     K     Z     g     v     ~     �     �  3   �     �  
   �  
   �     �     �               0   #      ,              4   5   @       .      $   8         -   /      1           (   "         2      +      :                  	   *   %   =       <                !       )       &       ;         
       7                                        ?             >   9                             3       '          6    "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-27 16:08+0100
Last-Translator: pixelcode <pixelcode@dismail.de>
Language-Team: Finnish <https://weblate.bubu1.eu/projects/sijacom/sijacom/fi/>
Language: fi
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" on 
yksinkertainen ja yksinkertainen staattinen 
verkkosivugeneraattori, jonka tarkoituksena on 
yksinkertaistaa staattisen verkkosivuston elementtien 
säätämistä eri useita alasivuja. Tutustu 
käyttöoppaaseen tietoa Sijacomin käytöstä.
 , Tietoja Sijacom Kaikki tiedostot on rakennettu onnistuneesti! Rakentaa kaikki Rakenna tiedostoja: Rakentaa tiedostoja? Bulgarian C:\asiakirjat\MinunHauskaProjekti\output\ C:\asiakirjat\MinunHauskaProjekti\root.json Peruutus Valitse tulostuskansio Valitse hakemisto Valitse tulostuskansio? Sulje Kokoaja: Vahvista Tanskaa Kehittäjä: Haluatko todella valita tämän tulostuskansion? Haluatko rakentaa valitut tiedostot? Haluatko ladata valitun juuritiedoston? Hollanti Englanti Virhe Viron Fatcow ikonit Tiedosto . Tiedostoa ei ole Kansiota ei ole Free Pascal Compiler Saksan Ikonit: Pidä Kieli LiJu09 Lataa päätiedosto? Logi Mondstern Tiedostoa ei ole valittu! Avaa kansio Lähtökansio valittu Output kansio: Pixelcode Puolan Viit. Vaihda Juuritiedosto ladattu Root-tiedosto: SOS pysäkki Valitse kaikki Sijacom Slovakia Menestys Poista kaikki Sinun on valittava juuritiedosto ja tulostuskansio! Infosivu Valitse... Valitse... Valitse Valitse Infoa v1.1 
��    B      ,  Y   <      �  �   �     �     �     �  	   �     �     �  	   �  #   �  %   !     G     N     f     w     �  	   �     �     �  
   �  0   �  (   �  +        =     C     K     Q     Z     g     l     �     �     �     �  	   �     �     �     �     �     �     �  	   �     �  	   	     	     1	  	   @	     J	     Q	     V	     ^	  
   o	     z	  
   �	     �	     �	     �	     �	  4   �	     �	  4   �	  ,   3
  1   `
  )   �
     �
     �
  {  �
    ]     k  
   m      x     �  
   �     �     �  $   �  &   �               0     <     P     T     `     i  	   o  0   y     �  )   �  	   �     �          	               "     4     :     M     b     g     p     x     }     �     �     �  	   �     �     �     �     �  	   �     �     �                  
   "  
   -     8  	   @  	   J     T  +   b     �     �     �     �     �     �     �     2   %      .              6   7   B       0      &   :         /   1       3           *   $         4      -      <                  	   ,   '   ?       >                "       +      (       =         
   #   9                        !               A             @   ;                             5       )          8    "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-27 16:07+0100
Last-Translator: pixelcode <pixelcode@dismail.de>
Language-Team: Danish <https://weblate.bubu1.eu/projects/sijacom/sijacom/da/>
Language: da
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining and Content Management" er en simpel 
statisk webstedsgenerator, hvis formål er at forenkle den 
processen med at justere statiske webstedselementer på tværs af flere undersider. Se venligst manualen for 
oplysninger om, hvordan du betjener Sijacom.
 , Om Sijacom Alle filer er bygget med succes! Byg alle Byg filer: Opbygge filer? Bulgarsk C:\documents\MitSjoveProjekt\output\ C:\documents\MitSjoveProjekt\root.json Annuller Vælg en output-mappe Vælg mappe Vælg output-mappe? Luk Kompilator: Bekræft Dansk Udvikler: Ønsker du virkelig at vælge denne outputmappe? Vil du bygge de valgte filer? Ønsker du at indlæse den valgte rodfil? Hollandsk Engelsk Fejl Estisk Fatcow-ikoner Fil Filen findes ikke Finsk Mappen findes ikke Free Pascal Compiler Tysk Islandsk Ikoner: Hold Sprog LiJu09 Indlæse rodfil? Logge Mondstern Ingen fil er valgt! Åbn fil Valgt udgangsmappe Output-mappe: Pixelcode Polsk Refer. Udskift Root-fil indlæst Rodfil: SOS stoppe Vælg alle Sijacom Slovakisk Vellykket Fravælg alle Du skal vælge en rodfil og en uddatamappe! Infos Vælg... Vælg... Vælg Vælg Infos v1.1 
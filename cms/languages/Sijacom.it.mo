��    G      T  a   �        �                  !  	   ?     I     V  	   c  #   m  %   �     �     �     �     �     �  	               
     0   '  (   X  +   �     �     �     �     �     �     �     �     �     		     	     '	     <	  	   C	     M	     T	     Y	     b	     i	     y	  	   }	     �	  	   �	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     
  
   
     
     
  	   &
     0
     8
     @
     ]
     j
  4   w
     �
  4   �
  ,   �
  1   %  )   W     �     �  }  �  7  $     \  
   ^  /   i     �     �     �     �  *   �  ,   �  
   +     6     T      k     �     �  
   �     �     �  1   �  "   �  '        =     F     N     U     \     i     n     ~  
   �     �     �     �  	   �     �     �     �     �     �       	        '     @     L     k  	        �     �  
   �     �     �     �     �     �     �     �     �          	     %     1  =   C     �     �     �  	   �  	   �     �     �        3                   @       9       2   F   G         6   /      ?   ,   A           
   D   '          8      >   	       +            :   1         %              "   &           <   )           C                         .       4         ;   #                  5   =   E   0      (                       B              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-30 16:02+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Italian <https://weblate.bubu1.eu/projects/sijacom/sijacom/it/>
Language: it
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" è un 
semplice generatore di siti web statici il cui scopo è 
quello di semplificare il processo di regolazione degli 
elementi statici del sito web attraverso sottopagine 
multiple. Si prega di fare riferimento al manuale per 
informazioni su come utilizzare Sijacom.
 , Su Sijacom Tutti i file sono stati costruiti con successo! Costruire tutti Costruire i file: Costruire file? Bulgaro C:\documents\MioProgettoDivertente\output\ C:\documents\MioProgettoDivertente\root.json Cancellare Scegli una cartella di output Scegliere la directory Scegliere la cartella di uscita? Chiudi Compilatore: Confermare Danese Sviluppatore: Vuoi davvero scegliere questa cartella di output? Vuoi costruire i file selezionati? Vuoi caricare il file root selezionato? Olandese Inglese Errore Estone Fatcow Icone Filo File non esiste Tipo di file non supportato! Finlandese Cartella non esiste Free Pascal Compiler Tedesco Islandese Icone: Tenere Lingua LiJu09 Caricare il file root? Registri Mondstern Nessun file selezionato! Aprire file Cartella di uscita selezionata Cartella di uscita: Pixelcode Polacco Rif. Sostituire File radice caricato File radice: SOS fermata Seleziona tutti Sijacom Slovacco Sloveno Successo Svedese Tradurre Sijacom su Weblate Traduttori: Deseleziona tutto Dovete scegliere un file principale e una cartella di output! Informa Scegliere... Scegliere... Seleziona Seleziona Informa v1.1 
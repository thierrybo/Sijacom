��    E      D  a   l      �  �   �     �     �       	        )     6  	   C  #   M  %   q     �     �     �     �     �  	   �     �     �  
   �  0     (   8  +   a     �     �     �     �     �     �     �     �     �     �     	     	  	   #	     -	     4	     9	     B	     I	     Y	  	   ]	     g	  	   y	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     
     
     +
     8
  4   E
     z
  4   �
  ,   �
  1   �
  )   %     O     o  }  t    �     �  
      "     	   .  
   8  
   C  
   N  %   Y  '        �     �     �     �     �     �  	   �     �       /        B  !   b     �     �     �     �     �     �     �     �     �     �     �       	        !     )     /     6     =     M  	   R     \  
   l     w     �  	   �     �     �     �     �  	   �  	   �  
   �     �  
   �  	   �          !     0  -   ?     m     t     }     �     �  	   �     �        3                   >       9       2   D   E         6   /      =   ,   ?           
   B   '          8      <   	       +                1         %              "   &               )           A                         .       4         :   #                  5   ;   C   0      (                       @              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-28 15:58+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Swedish <https://weblate.bubu1.eu/projects/sijacom/sijacom/sv/>
Language: sv
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining and Content Management" är ett enkelt 
program för att statisk webbplatsgenerator vars syfte är 
att förenkla processen att justera statiska 
webbplatselement över hela flera undersidor. Se 
manualen för information om hur du använder Sijacom.
 , Om Sijacom Alla filer byggdes framgångsrikt! Bygg alla Byggfiler: Byggfiler? Bulgariska C:\dokument\MittRoligaProjekt\output\ C:\dokument\MittRoligaProjekt\root.json Avbryt Välj en utdatamapp Välj katalog Välj utdatamapp? Stäng Kompilator: Bekräfta Danska Utvecklare: Vill du verkligen välja den här utdatamappen? Vill du bygga de valda filerna? Vill du ladda den valda rotfilen? Nederländska Engelska Fel Estniska Fatcow ikoner Fil Filen finns inte Filtyp som inte stöds! Finska Mappen finns inte Free Pascal Compiler Tyska Isländsk Ikoner: Håll Språk LiJu09 Ladda rotfilen? Logg Mondstern Ingen fil vald! Öppna fil Vald mapp för utdata Utdatamapp: Pixelcode Polsk Hän, Byt ut Root fil laddad Rotfilen: SOS stopp Välj alla Sijacom Slovakiska Framgång Översätt Sijacom på Weblate Översättare: Avmarkera alla Du måste välja en rotfil och en utdatamapp! Fråga Välj... Välj... Välj Välj Informera v1.1 